#include "stdafx.h"

#include <cassert>
#include <cstdio>
#include <Windows.h>
#include <cmath>

#include <tuple>

#include <cilk/cilk.h>
#include <cilk/reducer_opadd.h>


const double lbound = -1.0;
const double rbound = 1.0;
const size_t fractions = static_cast<size_t>(1.0e+8);

const double dx = (rbound - lbound) / fractions;

const double acceptable_error = 1.0e-7;
const double analytical_result = (M_PI + 2.0);


class timer
{
    size_t m_start, m_end;

public:
    void start() { m_start = GetTickCount(); }
    void stop() { m_end = GetTickCount(); }
    double get_measure()
    {
        return (m_end - m_start) * 1.0e-3;
    }
};

__inline double square(double arg)
{
    return arg * arg;
};

__inline double function(double arg)
{
    return 4. / square(1 + square(arg));
}

double intermediate_sum(size_t segment)
{
    return function(lbound + dx * segment) * dx;
}

std::tuple<double, double>
calculate_parallel(
    const double lbound,
    const double dx,
    const size_t fractions)
{
    cilk::reducer_opadd<double> value(0);
    timer t;

    t.start();
    cilk_for(size_t i = 0; i < fractions; ++i) {
        value += intermediate_sum(i);
    }
    t.stop();

    return std::make_tuple(value.get_value(), t.get_measure());
}

std::tuple<double, double>
calculate_serial(
    const double lbound,
    const double dx,
    const size_t fractions)
{
    double value(0);
    timer t;

    t.start();
    for (size_t i = 0; i < fractions; ++i) {
        value += intermediate_sum(i);
    }
    t.stop();

    return std::make_tuple(value, t.get_measure());
}

void assert_neighbour(
    double value,
    double x,
    double eps,
    const char *desc)
{
    assert((value > x - eps) && (value < x + eps) && desc);
}

double invoke(decltype(calculate_serial) func, const char *desc)
{
    double result, sec;

    std::tie(result, sec) = func(lbound, dx, fractions);

    assert_neighbour(result, analytical_result, acceptable_error, "analytical");
    printf("%s: value is %f, exec time is %fs\n", desc, result, sec);

    return result;
}

int main(int argc, char* argv[])
{
    double parallel_result, serial_result;

    parallel_result = invoke(&calculate_parallel, "parallel");
    serial_result = invoke(&calculate_serial, "serial");

    assert_neighbour(parallel_result, serial_result, acceptable_error, "serial");

    return 0;
}
